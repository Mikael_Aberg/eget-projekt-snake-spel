import java.awt.EventQueue;
import javax.swing.JFrame;

public class Snake extends JFrame {
	JFrame frame;
	private static final long serialVersionUID = 1L;
	
	public Snake(int players, boolean Ai) {

        add(new Board(players, Ai));
        
        setResizable(false);
        pack();
        
        setTitle("Snake");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }
	
    public static void main(String[] args) {
    	
    	
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StartMenu window = new StartMenu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
    	
    }
}