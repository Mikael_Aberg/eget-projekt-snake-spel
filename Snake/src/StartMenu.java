import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JRadioButton;
import javax.swing.JTextPane;

public class StartMenu{

	JFrame frame;
	
	public StartMenu(){
		initialize();
	}

	private void initialize() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JRadioButton AI_Button = new JRadioButton("AI");
		AI_Button.setBounds(6, 7, 109, 23);
		frame.getContentPane().add(AI_Button);
		
		JButton SPButton = new JButton("Single Player");
		SPButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				frame.removeAll();
				frame.dispose();
				
				System.out.println("SPButton pressed");
				EventQueue.invokeLater(new Runnable() {
					@Override
					public void run() {                
						JFrame frame = new Snake(1,AI_Button.isSelected());
						frame.setVisible(true);   
					}	
				});
			}
		});
		SPButton.setBounds(136, 7, 109, 23);
		frame.getContentPane().add(SPButton);
		
		JButton MPButton = new JButton("Multiplayer");
		MPButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				frame.removeAll();
				frame.dispose();
				
				System.out.println("MPButton pressed");
				EventQueue.invokeLater(new Runnable() {
					@Override
					public void run() {                
						JFrame frame = new Snake(2,AI_Button.isSelected());
						frame.setVisible(true);   
					}	
				});
			}
		});
		MPButton.setBounds(136, 41, 109, 23);
		frame.getContentPane().add(MPButton);
		
		JButton EXButton = new JButton("Exit");
		EXButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.removeAll();
				frame.dispose();
				System.exit(0);
				
			}
		});
		EXButton.setBounds(136, 75, 109, 23);
		frame.getContentPane().add(EXButton);
		
		JTextPane Inst = new JTextPane();
		Inst.setBackground(SystemColor.menu);
		Inst.setEditable(false);
		Inst.setFont(new Font("Tahoma", Font.BOLD, 13));
		Inst.setText("Instructions\r\n\r\nPlayer one :\r\nUp Arrow - Go up\r\nDown Arrow - go Down\r\nLeft Arrow - Go left\r\nRight Arrow - go Right\r\n\r\nPlayer two:\r\nW - Go up\r\nS - go Down\r\nA - Go left\r\nD - go Right");
		Inst.setBounds(267, 7, 157, 243);
		frame.getContentPane().add(Inst);
	}
}