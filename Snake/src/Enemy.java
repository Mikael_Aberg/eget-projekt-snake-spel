import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;

import javax.swing.ImageIcon;

public class Enemy {

	private final int x[] = new int[Board.getALL_DOTS()];
	private final int y[] = new int[Board.getALL_DOTS()];

	private int dots = 3;

	private int y_pos = y[0];
	private int x_pos = x[0];

	private boolean alive;

	private boolean left = false;
	private boolean right = true;
	private boolean up = false;
	private boolean down = false;

	private Image head;
	private Image body;

	public Enemy(Board board){
		loadImages();
		initEnemy();
	}

	//Ritar ut masken p� sin startplats
	private void initEnemy(){
		alive = true;
		for (int i = 0; i < dots; i++) {
			x[i] = 50 - i * 10;
			y[i] = 100;
		}

	}

	//Laddar in bilderna av masken
	private void loadImages(){

		ImageIcon iid_2 = new ImageIcon("Dot_2.png");
		body = iid_2.getImage();

		ImageIcon iih_1 = new ImageIcon("Head.png");
		head = iih_1.getImage();

	}

	//tar bort masken fr�n spelaplan
	public void dead(){
		dots = 0;
		alive = false;

	}

	//Ritar ut masken
	public void doDrawing(Graphics g, ImageObserver io){

		for(int i = 0; i < dots; i++){
			if(i == 0){
				g.drawImage(head, x[i], y[i],io);
			}else{
				g.drawImage(body, x[i], y[i],io);
			}
		}
	}

	//Kollar om masken f�r tag i �pplet
	public boolean checkApple(){

		if((x[0] == Board.getAppleX()) && (y[0] == Board.getAppleY())){
			dots++;
			return true;

		}else
			return false;
	}

	//Kollar ifall masken krockar i sig sj�lv eller spelaren
	private void checkCollision(){

		for (int i = dots; i > 0; i--) {	
			if ((x[0] == x[i]) && (y[0] == y[i]) 
					|| (x[0] == Board.getPlayerX(i,1) && (y[0] == Board.getPlayerY(i,1)))){
				dead();
			}
		}
	}

	//Styr maskens r�relser
	public void Move(){

		//K�rs s�l�nge masken lever
		if(alive){

			x_pos = x[0];
			y_pos = y[0];

			//Kollar om �pplet �r rakt ner�t
			if((x_pos == Board.getAppleX()) && (y_pos < Board.getAppleY())){
				tryDownTurn();
			}

			//kollar om �pplet �r rakt upp�t
			else if((x_pos == Board.getAppleX()) && (y_pos > Board.getAppleY())){
				tryUpTurn();
			}

			//kollar om �pplet �r rakt till v�nster
			else if((x_pos < Board.getAppleX()) && (y_pos == Board.getAppleY())){
				tryLeftTurn();
			}

			//Kollar om �pplet �r rakt till h�ger
			else if((x_pos > Board.getAppleX()) && (y_pos == Board.getAppleY())){
				tryRightTurn();
			}

			//kollar om �pplet �r n�gonstans �t v�nster
			else if(x_pos > Board.getAppleX()){
				tryLeftTurn();
			}

			//Kollar om �pplet �r n�gonstans �t h�ger
			else if(x_pos < Board.getAppleX()){
				tryRightTurn();
			}

			//Kollar om �pplet �r n�gonstans under
			else if(y_pos < Board.getAppleY()){
				tryDownTurn();
			}

			//Kollar om �pplet �r n�gonstans �ver
			else if(y_pos > Board.getAppleY()){
				tryUpTurn();

			}
			//Kollar om masken �r l�ngst ner p� spelplanen och p�v�g av den
			if ((down) && y[0] == Board.getHEIGHT() - 10) {
				tryLeftTurn();

			}
			//Kollar om masken �r h�gst upp p� sk�rmen och p�v�g av den
			if ((up) && (y[0] <= 0)) {
				tryRightTurn();

			}
			//Kollar om masken �r p� h�ger sida av sk�rmen och p�v�g av den
			if ((right) && x[0] == Board.getWIDTH() - 10){
				tryDownTurn();	

			}
			//Kollar om masken �r p� v�nster sida av sk�rmen och p�v�g av den
			if ((left) && (x[0] <= 0)) {
				tryUpTurn();

			}

			checkCollision();


			//skriver ut maskens x och y v�rde
			//System.out.println("x : " + x_pos + " y : " + y_pos);



			//L�gger in maskens plats i arrayerna
			for (int i = dots; i > 0; i--) {
				x[i] = x[(i - 1)];
				y[i] = y[(i - 1)];
			}

			//�ndrar maskens riktning
			if (left) {
				x[0] -= Board.getDOT_SIZE();
			}

			if (right) {
				x[0] += Board.getDOT_SIZE();
			}

			if (up) {
				y[0] -= Board.getDOT_SIZE();
			}

			if (down) {
				y[0] += Board.getDOT_SIZE();
			}

		}//while alive loop
	}//Move

	//Sv�ngar
	private void tryDownTurn(){

		if((!up)){
			down = true;
			right = false;
			left = false;
		}
	}

	private void tryUpTurn(){

		if((!down)){
			up = true;
			right = false;
			left = false;
		}
	}

	private void tryRightTurn(){

		if((!left)){
			up = false;
			right = true;
			down = false;
			left = false;
		}
	}

	private void tryLeftTurn(){

		if((!right)){
			up = false;
			left = true;
			down = false;
			right = false;
		}
	}

	//resetar den n�r man startar om spelet
	public void resetAI(){
		dots = 3;
		initEnemy();
	}

	//setter f�r att ge po�ng till masken
	public void givePoint(){dots++;}

	public int getX(int i){return x[i];}
	public int getY(int i){return y[i];}
	
}
