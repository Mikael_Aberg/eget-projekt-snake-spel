import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Board extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;

	private final static int WIDTH = 600;
	private final static int HEIGHT = 600;
	private final static int DOT_SIZE = 10;
	private final static int ALL_DOTS = 1200;
	private final int RAND_POS = 50;
	private final int DELAY = 100;
	private int players;

	private static int apple_x;
	private static int apple_y;

	public static Player player1;
	public static Player player2;

	Enemy e = new Enemy(this);

	private boolean inGame;
	private boolean Ai;

	private Timer timer;
	private Image apple;

	public Board(int players, boolean Ai) {
		this.players = players;
		this.Ai = Ai;
		player1 = new Player(1,50);
		player2 = new Player(2,100);


		addKeyListener(new TAdapter());
		setBackground(Color.black);
		setFocusable(true);

		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		loadImages();
		initGame();
		setVisible(true);
	}



	private void loadImages() {

		ImageIcon iia = new ImageIcon("Apple.png");
		apple = iia.getImage();

	}

	private void initGame() {

		inGame = true;

		locateApple();
		timer = new Timer(DELAY, this);
		timer.start();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		doDrawing(g);
	}

	private void doDrawing(Graphics g) {

		if (inGame) {

			g.setColor(Color.WHITE);

			player1.doDrawing(g, this);

			if(players == 2){player2.doDrawing(g, this);}

			if(Ai){e.doDrawing(g,this);}

			g.drawImage(apple, apple_x, apple_y, this);

			Toolkit.getDefaultToolkit().sync();

		} else {gameOver(g);}        
	}
	private void restart(int players){
		//restarts the game
		this.players = players;
		e.resetAI();
		player2.resetPlayer();
		player1.resetPlayer();
		initGame();

	}


	private void gameOver(Graphics g) {

		//Prints out score and instructions about restarting
		String msg = "Game Over";
		Font font = new Font("Helvetica", Font.BOLD, 14);
		FontMetrics metr = getFontMetrics(font);

		g.setColor(Color.white);
		g.setFont(font);
		g.drawString(msg, (WIDTH - metr.stringWidth(msg)) / 2, 300);
		g.drawString("Player 1 score : " + player1.getScore() ,(WIDTH - metr.stringWidth(msg)) / 2, 320);
		if(players == 2){g.drawString("Player 2 score : " + player2.getScore() ,(WIDTH - metr.stringWidth(msg)) / 2, 340);}
		g.drawString("R to restart with one player, T to restart with two players , Q to quit", 20, 20);
		g.drawString("Press x to toggle AI", 20, 50);
		g.drawString("AI is now : " + Ai,20,70);
	}

	private void checkApple() {

		if(player1.checkApple()){
			player1.giveScore();
			locateApple();
		}
		if(Ai){
			if(e.checkApple()){
				locateApple();
			}
		}
		if(players == 2){
			if(player2.checkApple()){
				player2.giveScore();
				locateApple();
			}
		}
	}

	private void move() {


		if(Ai){e.Move();}

		player1.move();

		if(players == 2){player2.move();}

	}

	private void checkCollision() {

		if(player1.checkCollision()){inGame = false;}

		if(players == 2){
			if(player2.checkCollision()){inGame = false;}
		}

		if(!inGame) {timer.stop();}
	}

	private void locateApple() {

		//puts the apple on a random location
		int r = (int) (Math.random() * RAND_POS);
		apple_x = ((r * getDOT_SIZE()));

		r = (int) (Math.random() * RAND_POS);
		apple_y = ((r * getDOT_SIZE()));
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (inGame) {
			checkApple();
			checkCollision();
			move();
		}

		repaint();
	}


	private class TAdapter extends KeyAdapter {

		@Override
		public void keyPressed(KeyEvent e) {

			int key = e.getKeyCode();
			//restart one player
			if((!inGame) && (key == KeyEvent.VK_R)){
				restart(1);

			}
			//restart two players
			if((!inGame) && (key == KeyEvent.VK_T)){
				restart(2);

			}
			//quit
			if((!inGame) && (key == KeyEvent.VK_Q)){
				System.exit(0);
			}
			
			//Toggle AI
			if((!inGame) && (key == KeyEvent.VK_X)){
				if(Ai){Ai = false;}
				else{Ai = true;}
				repaint();
			}
			

			//Player 1 kontroller
			if ((key == KeyEvent.VK_LEFT)) {
				player1.LeftTurn();
			}

			if ((key == KeyEvent.VK_RIGHT)) {
				player1.RightTurn();
			}

			if ((key == KeyEvent.VK_UP)) {
				player1.UpTurn();
			}

			if ((key == KeyEvent.VK_DOWN)) {
				player1.DownTurn();
			}

			//Player2 kontroller
			if(players == 2){
				if ((key == KeyEvent.VK_A)) {
					player2.LeftTurn();
				}

				if ((key == KeyEvent.VK_D)) {
					player2.RightTurn();
				}

				if ((key == KeyEvent.VK_W)) {
					player2.UpTurn();
				}

				if ((key == KeyEvent.VK_S)) {
					player2.DownTurn();
				}
			}
		}
	}

	//Getters
	public static int getPlayerX(int i, int playerNr){

		if(playerNr == 1){return player1.getPlayerX(i);}

		else{return player2.getPlayerX(i);}
	}

	public static int getPlayerY(int i, int playerNr){

		if(playerNr == 1){return player1.getPlayerY(i);}

		else{return player2.getPlayerY(i);}
	}
	
	public static int getHEIGHT(){return HEIGHT;}
	public static int getWIDTH(){return WIDTH;}
	public static int getAppleX(){return apple_x;}
	public static int getAppleY(){return apple_y;}
	public static int getALL_DOTS() {return ALL_DOTS;}
	public static int getDOT_SIZE() {return DOT_SIZE;}
}