import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;
import javax.swing.ImageIcon;

public class Player {

	private final int x[] = new int[Board.getALL_DOTS()];
	private final int y[] = new int[Board.getALL_DOTS()];

	private int dots;
	private int startPoint;
	private int playerNr;

	private int score;

	private boolean dead;
	private boolean left;
	private boolean right;
	private boolean up;
	private boolean down;

	private Image body_1;
	private Image body_2;
	private Image head_1;
	private Image head_2;

	public Player(int playerNr, int startPoint){
		this.startPoint = startPoint;
		this.playerNr = playerNr;
		loadImages();
		initPlayer();

	}

	private void loadImages() {

		if(playerNr == 1){

			ImageIcon iid_1 = new ImageIcon("Dot.png");
			body_1 = iid_1.getImage();

			ImageIcon iih_1 = new ImageIcon("Head.png");
			head_1 = iih_1.getImage();

		}else if(playerNr == 2){

			ImageIcon iid_2 = new ImageIcon("Dot_2.png");
			body_2 = iid_2.getImage();

			ImageIcon iih_2 = new ImageIcon("Head_2.png");
			head_2 = iih_2.getImage();
		}
	}

	private void initPlayer(){

		dead = false;
		score = 0;
		dots = 3;

		left = false;
		right = true;
		up = false;
		down = false;

		for (int i = 0; i < dots; i++) {
			x[i] = startPoint - i * 10;
			y[i] = startPoint;
		}
	}

	public void doDrawing(Graphics g,ImageObserver io){

		if(playerNr == 1){
			for (int i = 0; i < dots; i++) {
				if (i == 0) {
					g.drawImage(head_1, x[i], y[i], io);
				} else {
					g.drawImage(body_1, x[i], y[i], io);
				}
			}
			g.drawString("Player 1 score : " + score, 20, 20);
		}

		else if(playerNr == 2){
			for (int i = 0; i < dots; i++) {
				if (i == 0) {
					g.drawImage(head_2, x[i], y[i], io);
				} else {
					g.drawImage(body_2, x[i], y[i], io);
				}
			}
			g.drawString("Player 2 score : " + score, 500, 20);
		}
	}

	public void move(){

		for (int i = dots; i > 0; i--){
			x[i] = x[(i - 1)];
			y[i] = y[(i - 1)];
		}

		if (left) {
			x[0] -= Board.getDOT_SIZE();
		}

		if (right) {
			x[0] += Board.getDOT_SIZE();
		}

		if (up) {
			y[0] -= Board.getDOT_SIZE();
		}

		if (down) {
			y[0] += Board.getDOT_SIZE();
		}
	}

	public void resetPlayer(){
		initPlayer();
	}


	//Kollar om spelarner krockar i sig sj�lv eller den andra spelaren
	public boolean checkCollision() {

		if(playerNr == 1){
			for (int i = dots; i > 0; i--) {

				if ((x[0] == x[i]) && (y[0] == y[i])
						|| (x[0] == Board.getPlayerX(i, 2)) 
						&& (y[0] == Board.getPlayerY(i, 2))){
					dead = true;
				}
			}
		}else{
			for (int i = dots; i > 0; i--) {

				if ((x[0] == x[i]) && (y[0] == y[i])
						|| (x[0] == Board.getPlayerX(i, 1)) 
						&& (y[0] == Board.getPlayerY(i, 1))){
					dead = true;
				}
			}
		}

		//checks if the player goes outside the screen
		if (y[0] >= Board.getHEIGHT() + 10) {
			dead = true;
		}

		if (y[0] < 0) {
			dead = true;
		}

		if (x[0] >= Board.getWIDTH() + 10) {
			dead = true;
		}

		if (x[0] < 0) {
			dead = true;
		}
		return dead;
	}

	//Kollar om spelar f�tt tag i �pplet
	public boolean checkApple(){

		if((x[0] == Board.getAppleX()) && (y[0] == Board.getAppleY())){
			dots++;
			return true;
		}else
			return false;
	}

	//Sv�ngar
	public void LeftTurn(){
		if(!right){
			left = true;
			up = false;
			down = false;
		}
	}

	public void RightTurn(){
		if(!left){
			right = true;
			up = false;
			down = false;
		}
	}

	public void UpTurn(){
		if(!down){
			up = true;
			right = false;
			left = false;
		}
	}

	public void DownTurn(){
		if(!up){
			down = true;
			right = false;
			left = false;
		}
	}

	public void giveScore(){score++;}
	public int getScore(){return score;}
	public int getPlayerX(int i){return x[i];}
	public int getPlayerY(int i){return y[i];}

}
